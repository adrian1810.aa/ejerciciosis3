// Declarar un vector de 6 elementos
let vector = [1, 'dos', true, 4.5, 'cinco', false];

// a. Imprimir en la consola cada valor usando "for"
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b. Imprimir en la consola cada valor usando "forEach"
vector.forEach((valor) => {
  console.log(valor);
});

// c. Imprimir en la consola cada valor usando "map"
vector.map((valor) => {
  console.log(valor);
});

// d. Imprimir en la consola cada valor usando "while"
let i= 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

// e. Imprimir en la consola cada valor usando "for..of"
for (const valor of vector) {
  console.log(valor);
}